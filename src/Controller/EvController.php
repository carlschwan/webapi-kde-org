<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Ldap\Ldap;
use Symfony\Component\HttpFoundation\JsonResponse;

class EvController extends AbstractController
{
    /**
     * @Route("/ev/members", name="ev_members")
     */
    public function index(): JsonResponse
    {
        $ldap = Ldap::create('ext_ldap', [
            'connection_string' => $this->getParameter('ldap_url'),
        ]);

        $ldap->bind($this->getParameter('ldap_dn'), $this->getParameter('ldap_pw'));

        $query = $ldap->query('ou=people,dc=kde,dc=org', '(groupMember=ev-active)');
        $results = $query->execute()->toArray();

        usort($results, function (array $a, array$b): int {
            $a = $a['cn'][0];
            $b = $b['cn'][0];
            $replacements = ["å"=>"a", "à"=>"a", "é"=>"e"];
            $a = strtr(mb_strtolower($a, 'UTF-8'), $replacements);
            $b = strtr(mb_strtolower($b, 'UTF-8'), $replacements);
            return strcmp($a, $b);
        });

        $results = array_map(function($member) {
            return $member['cn'][0];
        }, $results);

        // result should be an array of name now

        return $this->json($results);
    }
}
